Rich data insights in blockchains

## chainchugger
- App to consume block chain data and decode transactions for logging
- To run
-- npm install
-- set the start and end block numbers you want to sync in the run function in index.js
-- npm run start

## ELK
Chainchugger is designed to work with elasticsearch and kibana
instructions to install locally https://www.elastic.co/start

### Filestore
abis and block data for blocks 8683991 -  8685535 is stored in fileStore.zip, the bootstrap from those files unzip and replace the contents of app/fileStore with the contents of the zipped fileStore and set the start block to 8683991 and end block to 8685536 and run the app