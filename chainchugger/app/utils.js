
const delay = function (msToDelay) {
  var promise = new Promise(function(resolve, reject) {
    setTimeout(function() {
      resolve('done!');
    }, msToDelay);
  });
  return promise;
}

const delayCall = function (boundFunc, msToDelay) {
  var promise = new Promise(function(resolve, reject) {
    setTimeout(boundFunc.bind(this, resolve), msToDelay);
  });
  return promise;
}


module.exports = {
  delay,
  delayCall
}