const rp = require('request-promise')
const decoder = require('abi-decoder')
const web3 = require('./web3')
const { ADDRESS_TYPES } = require('./constants')
const utils = require('./utils')
const { writeFile, readFileSync, readdir } = require('./file')


const ETHERSCAN_ABI_ENDPOINT = 'http://api.etherscan.io/api?module=contract&action=getabi&apikey='+process.env.ETHERSCAN_API_KEY+'&address='

const EXTERNAL_ADDRESS_CODE = '0x'
const abiStore = function () {
  const abis = {}
  return abis
}()

const fetchAbi = address => resolve => rp(`${ETHERSCAN_ABI_ENDPOINT}${address}`).then(response => resolve(response))

const getAbi = async address => {
  const addressStatus = abiStore[address]
  if (address == null) {
    console.log(`no address to look up`)
  }
  if (addressStatus) {
    console.log('abi already in system', address)
    return true
  }
  const addressCode = await web3.infuraProvider.getCode(address)
  const addressDetails = {
    abi: null,
    code: addressCode
  }

  if (addressCode === EXTERNAL_ADDRESS_CODE) {
    console.log(`${address} is ${ADDRESS_TYPES.EXTERNAL}`)
    abiStore[address] = ADDRESS_TYPES.EXTERNAL
    writeFile(`contract/address_${address}.json`, JSON.stringify(addressDetails))
    return ADDRESS_TYPES.EXTERNAL
  }
  const abiResponse = await utils.delayCall(fetchAbi(address), 250)
  const {status, result} = JSON.parse(abiResponse)

  // status of '0' means no verified abi
  if (status === '0') {
    console.log(`ABI status for address ${address} is ${ADDRESS_TYPES.CONTRACT_NO_ABI}`)
    writeFile(`contract/address_${address}.json`, JSON.stringify(addressDetails))
    abiStore[address] = ADDRESS_TYPES.CONTRACT_NO_ABI
    return ADDRESS_TYPES.CONTRACT_NO_ABI
  }
  console.log(`ABI status for address ${address} is ${ADDRESS_TYPES.CONTRACT_WITH_ABI}`)
  const abi = JSON.parse(result)
  decoder.addABI(abi)
  abiStore[address] = ADDRESS_TYPES.CONTRACT_WITH_ABI
  addressDetails.abi = JSON.parse(result)
  writeFile(`contract/address_${address}.json`, JSON.stringify(addressDetails))
  return ADDRESS_TYPES.CONTRACT_WITH_ABI
}

const initAbisFromFileStore = async () => {
  const files = await readdir('contract/')
  console.log('loading contracts from fileStore')
  files.forEach(file => {
    const address = file.split('_')[1].split('.')[0]
    const content = JSON.parse(readFileSync(`contract/${file}`))
    if (content.code === EXTERNAL_ADDRESS_CODE) {
      abiStore[address] = ADDRESS_TYPES.EXTERNAL
    }
    else if (content.abi == null) {
      abiStore[address] = ADDRESS_TYPES.CONTRACT_NO_ABI
    } else {
      abiStore[address] = ADDRESS_TYPES.CONTRACT_WITH_ABI
      decoder.addABI(content.abi)
    }
    console.log(`loaded address ${address} type ${abiStore[address]} from fileStore`)
  })
  console.log(`loaded ${files.length} contracts from fileStore - ${Object.keys(abiStore).length}`)
}

module.exports = {
  store: abiStore,
  getAbi,
  initAbis: initAbisFromFileStore
}