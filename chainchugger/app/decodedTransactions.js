const decoder = require('abi-decoder')
const { getBlockAndTransactions, getCode } = require('./nodeQueries')
const abi = require('./abi')
const { readFileSync, writeFile } = require('./file')
const { ADDRESS_TYPES } = require('./constants')


const formatEventArgs = eventArgs => eventArgs.map(arg => `${arg.name}:${arg.value}`).join('|')

const decodeLog = log => {
  const addressType = abi.store[log.address]
  if (addressType === ADDRESS_TYPES.CONTRACT_NO_ABI) {
    return Object.assign({}, log, {eventName: 'unknown'})
  }
  try {
    const decoded = decoder.decodeLogs([log])[0]
    const eventName = decoded ? decoded.name : 'unknown'
    const eventArgs = decoded ? formatEventArgs(decoded.events) : 'unknown'

    return {
      ...log,
      eventName,
      eventArgs
    }
  } catch(e) {
    console.log('decode log error ', log, e)
    return Object.assign({}, log, {eventName: 'unknown'})
  }
}

const decodeTransaction = transactionData => {
  try {
    const decoded = decoder.decodeMethod(transactionData)
    const transactionType = decoded ? decoded.name : 'unknown'
    const transactionArgs = decoded ? formatEventArgs(decoded.params) : 'unknown'
    return {
      transactionType,
      transactionArgs
    }
  } catch(e) {
    console.log('decode transaction error ', transactionData, e)
    return {transactionType: 'unknown'}
  }
}

decodeTransactionAndEvents = transaction => {
  const addressType = abi.store[transaction.to]
  return Object.assign(
    {},
    transaction,
    {
      addressType,
      logs: transaction.logs.map(decodeLog),
      decoded: abi.store[transaction.to] === ADDRESS_TYPES.EXTERNAL ? 
        {transactionType: 'Transfer'} :
        abi.store[transaction.to] === ADDRESS_TYPES.CONTRACT_WITH_ABI ?
          decodeTransaction(transaction.data) : {transactionType: 'unknown'}

    })
}

// get the block from a file if it exists, if not query the node
const getFullBlock = async blockNumber => {
  console.log('getting full block ', blockNumber)
  try {
    const blockFromFile = JSON.parse(readFileSync(`block/${blockNumber}.json`))
    console.log(' block from file', blockFromFile.block.hash)
    return blockFromFile
  } catch (e) {
    console.log('fetching block ', blockNumber)
    const fullBlock = await getBlockAndTransactions(blockNumber)
    await writeFile(`block/${blockNumber}.json`, JSON.stringify(fullBlock))
    return fullBlock
  }
}

const getDecodedBlock = async blockNumber => {
  const { block, transactions } = await getFullBlock(blockNumber)

  // find the address with no record
  const missingAbiAddresses = Array.from(new Set(transactions
    .map(trx => trx.to)
    .concat(
      transactions.reduce((acc, trx) => acc.concat(trx.logs.map(l=> l.address)), [])
    )))
    .filter(address => !abi.store[address])
    .filter(address => address)

  // try and get an abi for the missing addresses
  for (let address of missingAbiAddresses) {
    await abi.getAbi(address)
  }

  return { 
    block,
    transactions: transactions.map(decodeTransactionAndEvents)
  }
  
}

module.exports = {
  getDecodedBlock
}