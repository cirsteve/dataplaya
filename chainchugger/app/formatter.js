const constants = require('./constants')

const blockToESRecord = block => ({
  timestamp: block.timestamp,
  hash: block.hash,
  number: block.number,
  difficulty: block.difficulty,
  gasLimit: parseInt(block.gasLimit.toString()),
  gasUsed: parseInt(block.gasUsed.toString()),
  miner: block.miner,
  transactionCount: block.transactions.length
})

const transactionToESRecord = (block, transaction) => ({
  timestamp: block.timestamp,
  blockHash: block.hash,
  blockNumber: block.number,
  hash: transaction.hash,
  address: transaction.to,
  addressType: transaction.addressType,
  fromAddress: transaction.from,
  nativeValue: parseInt(transaction.value._hex, 16),
  gasPrice: parseInt(transaction.gasPrice._hex, 16),
  gasLimit: parseInt(transaction.gasLimit._hex, 16),
  gasUsed: parseInt(transaction.gasUsed._hex, 16),
  transactionType: transaction.decoded.transactionType,
  transactionArgs: transaction.decoded.transactionArgs 
})

const eventToESRecord = (block, transaction, event) => ({
  timestamp: block.timestamp,
  blockHash: block.hash,
  blockNumber: block.number,
  hash: transaction.hash,
  address: event.address,
  addressType: event.addressType,
  fromAddress: transaction.from,
  removed: event.removed,
  eventName: event.eventName,
  eventArgs: event.eventArgs 
})

const transactionToCsv = (transaction) =>
  [
    transaction.hash,
    transaction.address,
    transaction.addressType,
    transaction.from,
    transaction.value,
    transaction.gasPrice,
    transaction.gasLimit,
    transaction.gasUsed,
    constants.LOG_TYPES.TRANSACTION,
    transaction.decodedName,
    transaction.decodedParams
  ]

const eventToCsv = (event) =>
[
  event.transactionHash,
  transaction.address,
  transaction.addressType,
  transaction.from,
  transaction.value,
  transaction.gasPrice,
  transaction.gasLimit,
  transaction.gasUsed,
  constants.LOG_TYPES.TRANSACTION,
  transaction.decodedName,
  transaction.decodedParams
]

module.exports = {
  eventToCsv,
  transactionToCsv,
  blockToESRecord,
  transactionToESRecord,
  eventToESRecord
}