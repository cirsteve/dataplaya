'use strict'
const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

const saveDocument = (indexName, document) =>
  client.index({
    index: indexName,
    body: document
  })

module.exports = {
  saveDocument
}