const web3 = require('./web3')


const getCode = address => web3.infuraProvider.getCode(address)

const getBlock = blockNumber => web3.infuraProvider.getBlock(blockNumber)

const getTransactionReceipt = trxHash => web3.infuraProvider.getTransactionReceipt(trxHash)

const getTransaction = trxHash => web3.infuraProvider.getTransaction(trxHash)

const getTransactionInfo = async trxHash => {
  const receipt = await getTransactionReceipt(trxHash)
  const transaction = await getTransaction(trxHash)

  return Object.assign({}, receipt, transaction)
}

const getBlockAndTransactions = async blockNumber => {
  console.log('getting block, ', blockNumber)
  const block = await getBlock(blockNumber)
  const transactions = await Promise.all(block.transactions.map(getTransactionInfo))

  return {
    block,
    transactions
  }
}

module.exports = {
  getBlockAndTransactions,
  getCode,
  getBlock
}


