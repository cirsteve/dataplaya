require('dotenv').config()

const decodedTransactions = require('./decodedTransactions')
const { saveDocument } = require('./elasticsearch')
const formatter = require('./formatter')
const { initAbis } = require('./abi')

const getAndIndexBlocks = async (start, end) => {
  for (let  i = start; start < end;start++) {
    console.log('getting new block ', start, new Date().toLocaleTimeString())
    const { block, transactions } = await decodedTransactions.getDecodedBlock(start)
      .catch(e => console.log('error getting or decoding block', e))
    // console.log('got decoded: ', transactions)
    const savedBlock = await saveDocument('block', formatter.blockToESRecord(block))
      .catch(e => console.log(`error indexing block ${block.hash} ${e}`))
    //console.log('formated block ', formatter.blockToESRecord(block))
    console.log('saved block', savedBlock.body._id)
    await Promise.all(transactions.map(async (transaction) => {
      // console.log('trx doc, ',formatter.transactionToESRecord(block, transaction))
      const savedTrx = await saveDocument('transactions', formatter.transactionToESRecord(block, transaction))
        .catch(e => console.log(`error indexing transaction ${transaction.hash} ${e}`))
      const trxSaveSuccess = savedTrx ? savedTrx.body._id : `trx save error ${transaction.hash}`
      console.log('saved trx ', trxSaveSuccess)
      await Promise.all(transaction.logs.map(async log => {
        // console.log('log doc', formatter.eventToESRecord(block, transaction, log))
        const savedLog = await saveDocument('events', formatter.eventToESRecord(block, transaction, log))
        .catch(e => console.log(`error indexing event ${transaction.hash} ${e}`))
        console.log('saved log: ', savedLog.body._id)
      }))

    }))
    
    console.log('successfully indexed block', block.hash, block.number, new Date().toLocaleTimeString())
  }
  console.log('successfully indexed blocks range', start, end)
}
const blockNumber = 11393698//8684191
const run = async (start, end) => {
  await initAbis().catch(e => console.log(`error loading contracts from fileStore ${e}`))
  await getAndIndexBlocks(start, end)
  console.log(`indexed blocks ${start} to ${end}`)
  process.exit()
}

run(blockNumber-200, blockNumber+1)