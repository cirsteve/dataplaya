const fs = require('fs');

const writeFile = (path, content) => {
    return new Promise(function (resolve, reject) {
        fs.writeFile(
            `${__dirname}/fileStore/${path}`,
            content,
            (err, file) => err ? reject(err) : resolve(file)
        )
    })
}

const readFileSync = (path) => {
    return fs.readFileSync(`${__dirname}/fileStore/${path}`)
}

const readdir = async (path) => {
    return new Promise(function (resolve, reject) {
        fs.readdir(
            `${__dirname}/fileStore/${path}`,
            (err, files) => err? reject(err) : resolve(files)
            )
    })
}

module.exports = {
    writeFile,
    readFileSync,
    readdir
}