const Web3 = require('web3');
const localhostEndpoint = `http://localhost:8545`
const wsEndpoint = `wss://mainnet.infura.io/ws/v3/${process.env.INFURA_KEY}`
const ethers = require('ethers')
//const infuraProvider = new ethers.providers.JsonRpcProvider(localhostEndpoint, 'mainnet');
const infuraProvider = new ethers.providers.InfuraProvider('mainnet', process.env.INFURA_KEY);
//const web3 = new Web3(httpsEndpoint)
console.log('endpoint: ', wsEndpoint)
//const web3 = new Web3(new Web3.providers.WebsocketProvider(wsEndpoint));
const web3 = new Web3(infuraProvider)

module.exports = {
  web3,
  infuraProvider
}