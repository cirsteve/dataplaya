##ChainChugger

- Consume blockchain data, format it, and log it

Columns

blockNumber
blockTime
transactionHash
address
addressType
fromAddress
nativeValue
gasPrice
gasLimit
gasUsed
updateType // Transaction or Event
updateDetails // if decoded is the method name/event followed by key:value pairs of param updateName: {name}|key1:value1|key2:value2....





Sample Transaction/Log


Transaction Receipt 
{
   to:'0xB363A3C584b1f379c79fBF09df015DA5529d4dac',
   from:'0x140E683864FC714935Ed162b9aBf802fa7Bb5E58',
   contractAddress:null,
   transactionIndex:64,
   gasUsed:BigNumber   {
      _hex:'0xb3d3'
   },
   logsBloom:'0x00000000000000000000000000000000000000000000000000000400002000000020000000000000000000000000000000000200000000000000000000200000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000000000100000000400000000000000000000000000004000000000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000',
   blockHash:'0x2fdd987cce7d21dfaaabf4055933c0e40c80028e77a6c8a5cccfc5ca58890d9f',
   transactionHash:'0xda730e1786057813c4f215463249f862b93a8a5317e6965f3d3e7f0bb591c5ad',
   logs:[
      {
         transactionIndex:64,
         blockNumber:8465321,
         transactionHash:'0xda730e1786057813c4f215463249f862b93a8a5317e6965f3d3e7f0bb591c5ad',
         address:'0xB363A3C584b1f379c79fBF09df015DA5529d4dac',
         topics:[
            Array
         ],
         data:'0x00000000000000000000000000000000000000000000001fc462373d2ae80000',
         logIndex:60,
         blockHash:'0x2fdd987cce7d21dfaaabf4055933c0e40c80028e77a6c8a5cccfc5ca58890d9f',
         transactionLogIndex:0
      }
   ],
   blockNumber:8465321,
   confirmations:208641,
   cumulativeGasUsed:BigNumber   {
      _hex:'0x6f2d9b'
   },
   status:1,
   byzantium:true
}

Transaction
{  
   hash:'0xda730e1786057813c4f215463249f862b93a8a5317e6965f3d3e7f0bb591c5ad',
   blockHash:'0x2fdd987cce7d21dfaaabf4055933c0e40c80028e77a6c8a5cccfc5ca58890d9f',
   blockNumber:8465321,
   transactionIndex:64,
   confirmations:208641,
   from:'0x140E683864FC714935Ed162b9aBf802fa7Bb5E58',
   gasPrice:BigNumber   {  
      _hex:'0x47868c00'
   },
   gasLimit:BigNumber   {  
      _hex:'0x3d090'
   },
   to:'0xB363A3C584b1f379c79fBF09df015DA5529d4dac',
   value:BigNumber   {  
      _hex:'0x0'
   },
   nonce:269,
   data:'0x095ea7b30000000000000000000000008d12a197cb00d4747a1fe03395095ce2a5cc681900000000000000000000000000000000000000000000001fc462373d2ae80000',
   r:'0x7ea2a8a79769d4fc8a517723a4f9b88594c62b29cdd22181c786871e93af2e1f',
   s:'0x7b785613287799c65ef2b1350c26bac4167fc8d782db0eaaf074df29b7f94c7e',
   v:37,
   creates:null,
   raw:'0xf8ab82010d8447868c008303d09094b363a3c584b1f379c79fbf09df015da5529d4dac80b844095ea7b30000000000000000000000008d12a197cb00d4747a1fe03395095ce2a5cc681900000000000000000000000000000000000000000000001fc462373d2ae8000025a07ea2a8a79769d4fc8a517723a4f9b88594c62b29cdd22181c786871e93af2e1fa07b785613287799c65ef2b1350c26bac4167fc8d782db0eaaf074df29b7f94c7e',
   networkId:1,
   wait:[  
      Function
   ]
}

Decoded Transaction
{ name: 'approve',
  params:
   [ { name: '_spender',
       value: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
       type: 'address' },
     { name: '_value',
       value: '586000000000000000000',
       type: 'uint256' } ] }

Log
{ transactionIndex: 64,
  blockNumber: 8465321,
  transactionHash:
   '0xda730e1786057813c4f215463249f862b93a8a5317e6965f3d3e7f0bb591c5ad',
  address: '0xB363A3C584b1f379c79fBF09df015DA5529d4dac',
  topics:
   [ '0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925',
     '0x000000000000000000000000140e683864fc714935ed162b9abf802fa7bb5e58',
     '0x0000000000000000000000008d12a197cb00d4747a1fe03395095ce2a5cc6819' ],
  data:
   '0x00000000000000000000000000000000000000000000001fc462373d2ae80000',
  logIndex: 60,
  blockHash:
   '0x2fdd987cce7d21dfaaabf4055933c0e40c80028e77a6c8a5cccfc5ca58890d9f',
  transactionLogIndex: 0
}

Decoded Log
{ 
  name: 'Approval',
  events: [
    { 
      name: 'sender',
      type: 'address',
      value: '0x140e683864fc714935ed162b9abf802fa7bb5e58'
    },
    { 
      name: 'spender',
      type: 'address',
      value: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819'
    },
    { 
      name: 'value',
      type: 'uint256',
      value: '586000000000000000000'
    }
  ],
  address: '0xB363A3C584b1f379c79fBF09df015DA5529d4dac'
}
